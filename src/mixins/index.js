export const module_mixin={
    methods: {
        format_croatia_date(date){
            var data=new Date(date);
            var result=this.reformatMin(data.getDate()) +'/'+ (this.reformatMin(data.getMonth()+1)) +'/'+ data.getFullYear();

            return result;
        },
        format_croatia_date2(date){
            var data=new Date(date);
            var result=this.reformatMin(data.getDate()) +'/'+ (this.reformatMin(data.getMonth())) +'/'+ data.getFullYear();

            return result;
        },
        format_croatia_day(date){
            var data=new Date(date);
            var result=this.reformatMin(data.getDate()) ;
            
            return result;
        },
        reformatMin(min){
            return min.toString().length > 1 ? min+' ':'0'+min;
        },
        reformatTime(time){
            var hour=time.split(':')[0];
            var min=time.split(':')[1];
            hour=hour.length > 1 ? hour:'0'+hour.toString();
            min=min.length > 1 ? min:min.toString()+'0';
            
            return hour+':'+min;
        },
    },
}