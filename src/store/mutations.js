export default {
    setNotification(state,data){
        state.notification.type=data.type;
        state.notification.message=data.msg;

        if(!data.unset){
            setTimeout(()=>{
                state.notification.type=0;
            state.notification.message='';
            },2000);
        }
    },
    setLoader(state,data){
        state.loader=data;
    },
    setUser(state,data){
        state.user=data.user;
        state.token=data.token;
        
        localStorage.setItem('data',JSON.stringify({user:data.user,token:data.token}));
    },
    sortImages(state){
        state.home_picture=state.user.salon.images.find((d)=> d.type == 1);
        state.profile_picture=state.user.salon.images.find((d)=> d.type == 2);
        state.featured_picture=state.user.salon.images.filter((d)=> d.type == 3);

        state.home_picture=state.home_picture != undefined ? state.home_picture.filename:''
        state.profile_picture=state.profile_picture != undefined ? state.profile_picture.filename:'' 
    },
    logout(){
        localStorage.clear();
    }
}