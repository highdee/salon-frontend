import axios from 'axios';

export default {
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    // reset(context, endpoint){ 
    //     return new Promise((resolve,reject)=>{
    //         axios.get(endpoint)
    //         .then((data)=>{
    //             resolve(data)
    //         })
    //         .catch((error)=>{
    //             console.log('error', error.request); 
    //             // context.dispatch('checkIfSessionExpired',error);
    //             reject(error)
    //         })
    //     });
    // },
    delete(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.delete(endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    post(context , data){ 
        // console.log(context)
        // console.log(data)
        return new Promise((resolve,reject)=>{
            axios.post(data.endpoint, data.details,)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },

    put(context , data){ 
        return new Promise((resolve,reject)=>{
            axios.put(data.endpoint, data.details,)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },

    handleError(context,error){
        if(error.request.status == 422){
            var resp=JSON.parse(error.request.response);
            var err=resp.errors; 
            var msg='';
            for(var item in err){
                msg=err[item][0];
                break;
            } 
            context.commit('setNotification',{type:2,msg:msg});
            return msg;
        }
        else if(error.request.status == 404){
            resp=JSON.parse(error.request.response);
            msg= resp.message;
            context.commit('setNotification',{type:2,msg:msg}); 
        }
        else if(error.request.status == 401){
            msg="Oops! Authentication error, Please login again";
            context.commit('setNotification',{type:2,msg:msg});
            context.commit('logout');
            window.location.href="/";
        }
        else {
            msg="Oops! server error, Please try again";
            context.commit('setNotification',{type:2,msg:msg});
        }
    }
 
}