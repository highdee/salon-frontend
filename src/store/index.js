import Vue from 'vue';
import vuex from 'vuex';
import actions from './actions';
import mutations from './mutations';

Vue.use(vuex);
const store=new vuex.Store({
    state:{
        endpoint:'https://admin.villaperkovic.com/api/v1/',
        // endpoint:'http://localhost:8000/api/v1/',
        notification:{
            type:0,
            message:''
        },
        loader:false,
        image_viewer:false,
        active_image:0,
        user:null,
        token:null,
        home_picture:"",
        profile_picture:"",
        featured_picture:[],
        salons:[],
        salon:null,
        photos:[],
        booking_head:[],
        booking_body:[],
        comments:[]
    },
    actions,mutations
});


export default store;