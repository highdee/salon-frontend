import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

import home from './components/pages/home';
import biz from './components/pages/biz';
import salon_list from './components/pages/salon-list';
import salon from './components/pages/salon';
// import reset from './components/pages/reset';

import userDashboard from './components/pages/user/index';
import vendorDashboard from './components/pages/vendor/index';

const router =new Router({
    mode:'history',
    routes:[
        {
            path:'/',
            component:home
        },
        {
            path:'/business',
            component:biz
        },
        {
            path:'/salon-list',
            component:salon_list
        },
        {
            path:'/salon/:name',
            component:salon, 
        }, 
        {
            path:'/reset-password/:token',
            component:home, 
            name:'reset_password'
        },
        // {
        //     path:'/reset',
        //     component:reset, 
        //     name:'reset'
        // },
        {
            path:'/user',
            component:userDashboard,
            meta:{
                AuthRequired:true
            }
        },
        {
            path:'/vendor',
            component:vendorDashboard,
            meta:{
                AuthRequired:true
            }
        },
        {
            path:'/reset-password/:token',
            component:home, 
        },
    ]
});

export default router;

router.beforeEach((to,from,next)=>{
    if(to.matched.some(record => record.meta.AuthRequired)) {
        if (localStorage.getItem('data') == null) { 
            next({
                path: '/', 
            })
        } 
        else{ 
            var data=JSON.parse(localStorage.getItem('data'));
            console.log(to);
            if(to.path == '/vendor' && data.user.vendor == null){
                next({
                    path: '/user', 
                });
            }
            else if(to.path == '/user' && data.user.vendor != null){
                next({
                    path: '/vendor', 
                });
            }else{
                next(); 
            }
        }
    }
    
    next(); 
})