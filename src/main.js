import Vue from 'vue'
import App from './App.vue'

import router from './router';
import vuex from 'vuex';

import nav from './components/layouts/nav';
import footer from './components/layouts/footer';
import headerFixed from './components/layouts/header-fixed';
import store from './store/index';
import notification from "./components/layouts/notification";
import loader from "./components/layouts/loader";

Vue.component('home-nav',nav);
Vue.component('home-footer',footer);
Vue.component('header-fixed',headerFixed);
Vue.component('notify',notification);
Vue.component('loader',loader);

Vue.config.productionTip = false;
Vue.use(vuex);

new Vue({
  render: h => h(App),
  router,store,
}).$mount('#app')
