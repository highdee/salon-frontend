/* datepicker */
    $(document).ready(function() {
        $('#datepicker').datepicker({
            language: "hr",
            format: 'yyyy-dd-mm',
            startDate: '1d'
        });
        // $('#datepicker').on('changeDate', function() {
        //     $('#bookModal').modal('hide');
        //     $('#terminModal').modal('show');
        // });
    });

$(document).ready(function() {
    "use strict";


    var window_width = $(window).width(),
        window_height = window.innerHeight,
        header_height = $(".default-header").height(),
        header_height_static = $(".site-header.static").outerHeight(),
        fitscreen = window_height - header_height;

    $(".fullscreen").css("height", window_height)
    $(".fitscreen").css("height", fitscreen);


    //------- Niceselect  js --------//  

    if (document.getElementById("default-select")) {
        $('select').niceSelect();
    };
    if (document.getElementById("default-select2")) {
        $('select').niceSelect();
    };
    if (document.getElementById("service-select")) {
        $('select').niceSelect();
    };    

    //------- Lightbox  js --------//  

    $('.img-gal').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    $('.play-btn').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    //------- Superfish nav menu  js --------//  

    $('.nav-menu').superfish({
        animation: {
            opacity: 'show'
        },
        speed: 400
    });

    //------- Owl Carusel  js --------//  

    $('.active-recent-blog-carusel').owlCarousel({
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.owl-container .custom-nav',
            items: 4,
            loop: true,
            margin: 0,
            dots: true,
            autoplayHoverPause: true, 
            smartSpeed:300,               
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2,
                },
                768: {
                    items: 3,
                },
                992: {
                    items: 4,
                },
                1200: {
                    items: 4
                }
            }
        });
        
    $('.salon-gallery-carousel').owlCarousel({
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.owl-container .custom-nav',
            items: 4,
            loop: true,
            margin: 0,
            dots: true,
            autoplayHoverPause: true, 
            smartSpeed:300,               
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1,
                },
                768: {
                    items: 2,
                },
                961: {
                    items: 3,
                }
            }
        });
        
    $('.owl-one').owlCarousel({
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.owl-container .customer-nav',
            items: 4,
            loop: true,
            margin: 0,
            dots: true,
            autoplayHoverPause: true, 
            smartSpeed:300,               
            autoplay: true,
            animateOut: 'fadeOut',
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1,
                },
                768: {
                    items: 1,
                },
                961: {
                    items: 1,
                }
            }
        });


    //------- Smooth Scroll js --------//  

    $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                var top_space = 0;

                if ($('#header').length) {
                    top_space = $('#header').outerHeight();

                    if (!$('#header').hasClass('header-fixed')) {
                        top_space = top_space;
                    }
                }

                $('html, body').animate({
                    scrollTop: target.offset().top - top_space
                }, 1500, 'easeInOutExpo');

                if ($(this).parents('.nav-menu').length) {
                    $('.nav-menu .menu-active').removeClass('menu-active');
                    $(this).closest('li').addClass('menu-active');
                }

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('lnr-times lnr-bars');
                    $('#mobile-body-overly').fadeOut();
                }
                return false;
            }
        }
    });

    $(document).ready(function() {

        $('html, body').hide();

        if (window.location.hash) {

            setTimeout(function() {

                $('html, body').scrollTop(0).show();

                // $('html, body').animate({

                //     scrollTop: $(window.location.hash).offset().top - 108

                // }, 1000)

            }, 0);

        } else {

            $('html, body').show();

        }

    });


    jQuery(document).ready(function($) {
        // Get current path and find target link
        var path = window.location.pathname.split("/").pop();

        // Account for home page with empty path
        if (path == '') {
            path = 'index.html';
        }

        var target = $('nav a[href="' + path + '"]');
        // Add active class to target link
        target.addClass('menu-active');
    });

    $(document).ready(function() {
        if ($('.menu-has-children ul>li a').hasClass('menu-active')) {
            $('.menu-active').closest("ul").parentsUntil("a").addClass('parent-active');
        }
    });

    // smooth scroll
    var scroll = new SmoothScroll('a[href*="#"]');



    //------- Header Scroll Class  js --------//  

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('page-scrolled');
            $('#logo-title').css("color", "#000");
            $('#navbar-toggler').css("color", "#000");
        } else {
            $('#header').removeClass('page-scrolled'); 
            $('#logo-title').css("color", "#fff");
            $('#navbar-toggler').css("color", "#fff");
        }
    });
    
    
    $('#navbar-toggler').click(function() {
        if ($(window).scrollTop() < 100){
            if($('#header').hasClass('page-scrolled')) {
                $('#header').removeClass('page-scrolled');
                $('#logo-title').css("color", "#fff");
                $('#navbar-toggler').css("color", "#fff");
            }else {
                $('#header').addClass('page-scrolled');
                $('#logo-title').css("color", "#000");
                $('#navbar-toggler').css("color", "#000");
            }
        }
    });
    
    // show more services on salon.php
    
    $('#collapseTableBtn').click(function() {
        if ($("#collapseTable").hasClass('show')) {
            $('#collapseTableBtn').html('Prikaži više');
        } else {
            $('#collapseTableBtn').html('Prikaži manje');
        }
    });
    
    //------- Google Map  js --------//  

    if (document.getElementById("map")) {
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400), // New York
                styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }]
            };
            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6700, -73.9400),
                map: map,
                title: 'Snazzy!'
            });
        }
    }

    //------- Mailchimp js --------//  

    // $(document).ready(function() {
    //     $('#mc_embed_signup').find('form').ajaxChimp();
    // });
    
    /* Masonry
    * ---------------------------------------------------- */ 
    var clMasonryFolio = function () {
        
        var containerBricks = $('.masonry');

        containerBricks.imagesLoaded(function () {
            containerBricks.masonry({
                itemSelector: '.masonry__brick',
                percentPosition: true,
                resize: true
            });
        });

    };
    
});


 


// Counter
(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 3000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
    
    var runOnce = 0;
    $(window).scroll(function(){
        if ($('.paralsec').isOnScreen() && runOnce == 0) {
            $('.timer').each(count);
            runOnce++;
        }
    });
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});

$.fn.isOnScreen = function(){
    
    // var win = $(window);
    
    // var viewport = {
    //     top : win.scrollTop(),
    //     left : win.scrollLeft()
    // };
    // viewport.right = viewport.left + win.width();
    // viewport.bottom = viewport.top + win.height();
    
    // var bounds = this.offset();
    // bounds.right = bounds.left + this.outerWidth();
    // bounds.bottom = bounds.top + this.outerHeight();
    
    // return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    
};


// COOKIES CONSENT

(function () {
    "use strict";

    setTimeout(()=>{
        var cookieAlert = document.querySelector(".cookiealert");
        console.log(cookieAlert);
        var acceptCookies = document.querySelector(".acceptcookies");
    
        cookieAlert.offsetHeight; // Force browser to trigger reflow (https://stackoverflow.com/a/39451131)
    
        if (!getCookie("acceptCookies")) {
            cookieAlert.classList.add("show");
        }
    
        acceptCookies.addEventListener("click", function () {
            setCookie("acceptCookies", true, 60);
            cookieAlert.classList.remove("show");
        });
    },100);
})();

// Cookie functions from w3schools
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// stavi footer na dno stranice

$(document).ready(function () {
    setInterval(function () {
        var docHeight = $(window).height();
        var footerHeight = $('footer').height();
        var footerTop = $('footer').position().top + footerHeight;
        var marginTop = (docHeight - footerTop);

        if (footerTop < docHeight)
            $('footer').css('margin-top', marginTop + 'px'); // padding of 30 on footer
        else
            $('footer').css('margin-top', '0px');
        // console.log("docheight: " + docHeight + "\n" + "footerheight: " + footerHeight + "\n" + "footertop: " + footerTop + "\n" + "new docheight: " + $(window).height() + "\n" + "margintop: " + marginTop);
    }, 250);
});


//  new Chart(document.getElementById("rezervacije-chart"), {

//         type: 'line',
//         data: {
//           labels: ['January', 'February', 'March', 'April', 'May', 'June'],
//           type: 'line',
//           datasets: [{
//             data: [1, 18, 9, 17, 34, 22],
//             label: 'Dataset',
//             backgroundColor: 'transparent',
//             borderColor: 'rgba(255,255,255,.55)',
//           },]
//         },
//         options: {

//           maintainAspectRatio: true,
//           legend: {
//             display: false
//           },
//           responsive: true,
//           tooltips: {
//             mode: 'index',
//             titleFontSize: 12,
//             titleFontColor: '#000',
//             bodyFontColor: '#000',
//             backgroundColor: '#fff',
//             titleFontFamily: 'Montserrat',
//             bodyFontFamily: 'Montserrat',
//             cornerRadius: 3,
//             intersect: false,
//           },
//           scales: {
//             xAxes: [{
//               gridLines: {
//                 color: 'transparent',
//                 zeroLineColor: 'transparent'
//               },
//               ticks: {
//                 fontSize: 2,
//                 fontColor: 'transparent'
//               }
//             }],
//             yAxes: [{
//               display: false,
//               ticks: {
//                 display: false,
//               }
//             }]
//           },
//           title: {
//             display: false,
//           },
//           elements: {
//             line: {
//               tension: 0.00001,
//               borderWidth: 1
//             },
//             point: {
//               radius: 4,
//               hitRadius: 10,
//               hoverRadius: 4
//             }
//           }
//         }
// });

//  new Chart(document.getElementById("sati-rada-chart"), {
    
    
//         type: 'line',
//         data: {
//             labels: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj'],
//              type: 'line',
//             datasets: [{
//                 data: [78, 81, 80, 45, 34, 12, 40],
//                 label: 'Rezervacije',
//                 backgroundColor: 'rgba(255,255,255,.1)',
//                 borderColor: 'rgba(255,255,255)',
//               }
//             ]
//         },
//         options: {
//           maintainAspectRatio: true,
//           legend: {
//             display: false
//           },
//           layout: {
//             padding: {
//               left: 0,
//               right: 0,
//               top: 0,
//               bottom: 0
//             }
//           },
//           responsive: true,
//           scales: {
//             xAxes: [{
//               gridLines: {
//                 color: 'transparent',
//                 zeroLineColor: 'transparent'
//               },
//               ticks: {
//                 fontSize: 2,
//                 fontColor: 'transparent'
//               }
//             }],
//             yAxes: [{
//               display: false,
//               ticks: {
//                 display: false,
//               }
//             }]
//           },
//           title: {
//             display: false,
//           },
//           elements: {
//             line: {
//               borderWidth: 0
//             },
//             point: {
//               radius: 0,
//               hitRadius: 10,
//               hoverRadius: 4
//             }
//           }
//         }
// });

//  new Chart(document.getElementById("prihodi-chart"), {
    
    
//         type: 'bar',
//         data: {
//           labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
//           datasets: [
//             {
//               label: "My First dataset",
//               data: [78, 81, 80, 65, 58, 75, 60, 75, 65, 60, 60, 75],
//               borderColor: "transparent",
//               borderWidth: "0",
//               backgroundColor: "rgba(255,255,255,.3)"
//             }
//           ]
//         },
//         options: {
//           maintainAspectRatio: true,
//           legend: {
//             display: false
//           },
//           scales: {
//             xAxes: [{
//               display: false,
//               categoryPercentage: 1,
//               barPercentage: 0.65
//             }],
//             yAxes: [{
//               display: false
//             }]
//           }
//         }
// });












